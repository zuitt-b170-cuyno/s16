function removeVowel(string) {
    const length = string.length, newString = []
    for (let i = 0; i < length; i++) {
        if (isVowel(string[i])) continue
        newString.push(string[i])
    }

    return newString.join("")
}

function isVowel(alphabet) {
    return alphabet.toLowerCase() === "a" || alphabet.toLowerCase() === "e" ||
           alphabet.toLowerCase() === "i" || alphabet.toLowerCase() === "o" ||
           alphabet.toLowerCase() === "u"
}

let number = Number(prompt("Enter a number"))
console.log(`The number you provided is ${number}.`)
for (; number > 0; number--) {
    if (number <= 50) {
        console.log(`The current value is at ${number}. Terminating the loop.`)
        break
    }

    if (number % 10 === 0) 
        console.log("The number is divisible by 10. Skipping the number.")
    else if (number % 5 === 0)
        console.log(number)
}

const string = "supercalifragilisticexpialidocious"
const newString = removeVowel(string)
console.log(string)
console.log(newString)