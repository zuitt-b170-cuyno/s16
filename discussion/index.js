// While Loop

let count = 5
while (count >= 0) {
    console.log(`While loop: ${count}`)
    count--
}

let countTwo = 1
while (countTwo < 11) 
    console.log(countTwo++)

// Do-While Loop

let countThree = 5
do {
    console.log(`Do-While Loop: ${countThree--}`)
} while (countThree > 0)

let countFour = 1
do {
    console.log(countFour++)
} while (countFour < 11)

// For Loop

for (let countFive = 5; countFive > 0; countFive--) 
    console.log(`For Loop: ${countFive}`)

let number = Number(prompt("Give me a number:"))
for (let numCount = 1; numCount <= number; numCount++) 
    console.log("Hello Batch 170")

const myName = "Alex"
console.log(myName.length)
console.log(myName[2])

for (let x = 0; x < myName.length; x++)
    console.log(myName[x])

for (let x = 0; x < myName.length; x++) 
    isVowel(myName[x]) ? console.log(3) : console.log(myName[x])
    
function isVowel(alphabet) {
    return alphabet.toLowerCase() === "a" || alphabet.toLowerCase() === "e" ||
           alphabet.toLowerCase() === "i" || alphabet.toLowerCase() === "o" ||
           alphabet.toLowerCase() === "u"
}

for (let countSix = 0; countSix <= 20; countSix++) {
    if (countSix % 2 === 0) continue
    console.log(`Continue and Break: ${countSix}`)
    if (countSix > 10) break
}

let name = "Alexandro"
for (let i = 0; i < name.length; i++) {
    console.log(name[i])
    if (name[i].toLowerCase() === "a") {
        console.log("Continue to the next iteration")
        continue
    }

    if (name[i].toLowerCase() === "d") break
}